<?php

namespace App\Http\Controllers;

use App\Components\SimBaseAuth;
use Illuminate\Http\Request;

class AccommodationsController extends Controller
{
    /** @var SimBaseAuth */
    protected $client;

    public function __construct(SimBaseAuth $client)
    {
        $this->client = $client;
    }

    /**
     * Возвращает список параметров Status, Accommodation, Room Type
     * @return array|int[]
     */
    public function data()
    {
        return $this->client->callFunction('f_api_return_accommodations_data');
    }

    public function store(Request  $request)
    {
        $this->validate($request, [
            'acc_id' => 'required|integer',
            'number' => 'required|integer|min:1',
            'room_type' => 'required|integer',
            'check_in' => 'required|integer',
            'nights' => 'required|integer|min:1'
        ]);
        return $this->client->callFunction('f_api_save_accommodations_data', [
            'acc_id' => $request->get('acc_id'),
            'number' => $request->get('number'),
            'room type' => $request->get('room_type'),
            'check in' => $request->get('check_in'),
            'nights' => $request->get('nights')
        ]);

    }
}
