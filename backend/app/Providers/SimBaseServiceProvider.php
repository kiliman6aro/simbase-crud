<?php

namespace App\Providers;

use App\Components\SimBaseAuth;
use Illuminate\Support\ServiceProvider;

class SimBaseServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('App\Components\SimBaseAuth', function (){
            //Костыль, так как внутри API идет обращение к данной переменной, которой может и не быть
            //если, например, запрос к API выполняется из консольного приложения. Кроме того, опытным путем,
            //было установленно, что запрос выполняется только если REMOTE_ADDR стоит как 127.0.0.1
            $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
            return new SimBaseAuth();
        });
    }
}
