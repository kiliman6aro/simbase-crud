# Deploy

## Backend configure
* cd backend
* composer install
* php artisan key:generate
* cp .env.example .env
* set SIMBASE config in .env file


## Frontend configure
* cd frontend
* npm install
* npm run build 


## Composer configure
* cp fronted/docker/nginx/default.example.conf fronted/docker/nginx/default.conf
* set configure nginx fronted/docker/nginx/default.conf
* cp backend/docker/nginx/default.example.conf backend/docker/nginx/default.conf
* set configure nginx backend/docker/nginx/default.conf
docker-compose up -d
